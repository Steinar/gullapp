                                    gullapp

A collection of scripts and utilities to help do note taking. Gullapp, as in
gul-lapp, is Norwegian for “yellow note”, the most common colloquial term for
sticky notes in Norwegian.


    1.0 gullapp

Create or list notes in a directory pointed to by the environment variable named
GULLAPP_DIR. Setting the variable is mandatory for the program to work, there is
no default value.

gullapp has four basic functions:

1. Create new notes, either with an editor or reading straight from STDIN.
2. List the newest notes, “current work context”.
3. List all notes.
4. List all already existing note categories.

Note categories are simply directories.

Run with “-h” for a synopsis on usage.


    2.0 namefromfirstline

Create a unique file name based on the first line of data from STDIN. Used to
avoid having to create file names manually for new notes. The script is mostly
intended as a pure piece of support tooling for gullapp.

Run with “-h” for a synopsis on usage.


    3.0 dato

Shorthand for getting current date in different formats. Run with “-h” for a
synopsis on usage.


    4.0 Licence

Copyright 2022 Steinar Knutsen

Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the
European Commission – subsequent versions of the EUPL (the “Licence”); You may
not use this work except in compliance with the Licence. You may obtain a copy
of the Licence at:

https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12

Unless required by applicable law or agreed to in writing, software distributed
under the Licence is distributed on an “AS IS” basis, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the Licence for the
specific language governing permissions and limitations under the Licence.
